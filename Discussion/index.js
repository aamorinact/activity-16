// alert ('Pakyu')

let count = 5;

// While loop
	// Syntax: 
	// while(expression/condition){
	// 		statement/ block of codes
	// }

while(count !== 0){
	console.log("While:" + count)

	count--
};

/*Syntax:
	do {
		statement
	} while(expression/condition)


*/

let number = Number(prompt('Give me a number'))
do {
	console.log("Do While" + number)
	number += 1
} while(number < 10)

// For Loops
// 3 parts of for loops
	// initialization
	// expression / condition
	//iteration

/*
	Syntax:
	for(initialization; expression/condition;
	iteration){
		statement/ code block
	}

*/


for(let count = 0; count <= 20; count++){
	console.log(count);
};

let myString = "archee";

console.log(myString.length);

console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);
console.log(myString[4]);
console.log(myString[5]);

for(let x = 0; x < myString.length; x++){
	console.log(myString[x])
};

let myName = "archee"

for(let i = 0; i < myName.length; i++){
	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 


	){
			console.log(3);
	} else {
			console.log(myName[i]);
	};

}; 

// Continue and Break

for(let count = 0; count <= 20; count++){
	if( count % 2 === 0){
		continue;
	}

	console.log("Continue and Break:" + count)

	if(count > 10){
		break;
	}
}